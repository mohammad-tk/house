from django.contrib import admin
from .models import OtpNumber


from django.contrib.auth import get_user_model
User = get_user_model()

class UserAdmin(admin.ModelAdmin):
    list_display = (
        'phone',
        'email',
        'first_name',
        'last_name',
        'is_staff',
        'is_active',
        'is_superuser',
        'last_login',
    )
    search_fields = ('phone',)
    list_filter = ('is_staff', 'is_active', 'is_superuser')


admin.site.register(User, UserAdmin)


class OtpNumberAdmin(admin.ModelAdmin):
    list_display = (
        'phonenumber',
        'otp_number',
        'counts',
        'validate',
    )
    search_fields = ('phonenumber',)
    list_filter = ('counts', 'validate',)


admin.site.register(OtpNumber, OtpNumberAdmin)

