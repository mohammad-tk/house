from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.decorators import login_required
from .models import User, OtpNumber
import random
from kavenegar import *
from .serializer import CreateUserSerializer, LoginSerializer, UserSerializer, bookmarkserializer
from knox.views import LoginView as KnoxLoginView
from knox.models import AuthToken
from django.contrib.auth import login
import re
from base.models import Bookmark

reg = "^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"
match_re = re.compile(reg)


def register_page(request):  # registeration page.
    return render(request, 'registreation.html')


def passwordrecovery_page(request):  # password recovery page.
    return render(request, 'passwordrecovery.html')


def login_page(request):  # login user page.
    return render(request, 'login.html')


def set_password(request):  # set password page.
    return render(request, 'set_password.html')


def new_password_replace(request):  # set new password.
    return render(request, 'new_password_replace.html')


@login_required(login_url='/loginuser/')
@permission_classes([IsAuthenticated])
def dashboard_page(request):  # user panel page.
    return render(request, 'dashboard.html')


@ api_view(['POST'])
@permission_classes([AllowAny])
def phone_validation(request):  # send otp code to phonenumber.
    phonenumber = request.POST.get('phonenumber')
    if phonenumber:
        user = User.objects.filter(phone__iexact=phonenumber)
        if user.exists():
            return Response({
                'status': False,
                'detail': 'این شماره تلفن قبلا در سایت ثبت نام شده است.'
            })
        else:
            key = send_otp(phonenumber)
            if key:
                obj = OtpNumber.objects.filter(phonenumber=phonenumber)
                if obj:
                    objcount = obj.first()
                    if objcount.counts < 7:
                        # api = KavenegarAPI(
                        #     '7952507248584D75533155316553305A71497849324973554D465645566E474851547333537367396E56303D')
                        # params = {
                        #     'receptor': phonenumber,
                        #     'token': key,
                        #     'template': 'Verify',
                        #     'message': 'Kaveh specialized Web service '
                        # }
                        # response = api.verify_lookup(params)
                        objcount.counts += 1
                        objcount.otp_number = key
                        objcount.save()
                        return Response({
                            'status': True,
                            'detail':
                            'کد تایید ارسال شد.'
                        })
                    else:
                        return Response({
                            'status': False,
                            'detail':
                            'تعداد درخواست های تایید هویت پیامکی بیش از حد مجاز است لطفا با بخش پشتیبانی تماس فرمایید.'
                        })
                else:
                    # api = KavenegarAPI(
                    #     '7952507248584D75533155316553305A71497849324973554D465645566E474851547333537367396E56303D')
                    # params = {
                    #     'receptor': phonenumber,
                    #     'token': key,
                    #     'template': 'Verify',
                    #     'message': 'Kaveh specialized Web service '
                    # }
                    # response = api.verify_lookup(params)
                    OtpNumber.objects.create(
                        phonenumber=phonenumber, otp_number=key)
                    return Response({
                        'status': True,
                        'detail':
                            'کد تایید ارسال شد.'
                    })
            else:
                return Response({
                    'status': False,
                    'detail': 'send otp error.'
                })
    else:
        return Response({
            'status': False,
            'detail': 'شماره تلفن وارد نشده است.'
        })


def send_otp(phone):  # create otp number.
    if phone:
        key = random.randint(999, 9999)
        return key
    else:
        return False


@ api_view(['POST'])
@permission_classes([AllowAny])
def otp_validator(request):  # validation otp code With phonenumber.
    otp_number = request.POST.get('otp')
    phonenumber = request.POST.get('phonenumber')
    if otp_number and phonenumber:
        phone = OtpNumber.objects.filter(
            phonenumber=phonenumber, otp_number=otp_number)
        obj = phone.first()
        if obj and obj.otp_number == otp_number:
            obj.validate = True
            obj.save()
            return Response({
                'status': True,
                'detail': 'کاربر با موفقیت ثبت نام شد.'
            })
        else:
            return Response({
                'status': False,
                'detail': 'کد تایید منطبق نیست.'
            })
    else:
        return Response({
            'status': False,
            'detail': 'کد تایید وارد نشده است.'
        })


@ api_view(['POST'])
@permission_classes([AllowAny])
def registeration(request):  # set password and user registeration.
    password = request.POST.get('password')
    phonenumber = request.POST.get('phonenumber')
    if phonenumber and password:
        res = re.search(match_re, password)
        if res:
            myobj = OtpNumber.objects.filter(phonenumber=phonenumber)
            obj = myobj.first()
            if obj:
                if obj.validate:
                    temp_data = {
                        'phone': phonenumber,
                        'password': password,
                    }
                    serializer = CreateUserSerializer(data=temp_data)
                    serializer.is_valid(raise_exception=True)
                    user = serializer.save()
                    obj.delete()
                    if user:
                        return Response({
                            'status': True,
                            'user': UserSerializer(user).data,
                            'token': AuthToken.objects.create(user)[1],
                            'detail': 'کاربر با موفقیت ثبت نام شد.'
                        })
                    else:
                        return Response({
                            'status': False,
                            'detail': 'اوه اوه یه مشکلی پیش اومده.'
                        })
                else:
                    return Response({
                        'status': False,
                        'detail': 'otp تایید نشده است.'
                    })
            else:
                return Response({
                    'status': False,
                    'detail': 'شماره موبایل پیدا نشد.'
                })
        else:
            return Response({
                'status': False,
                'detail': 'طول پسورد باید حداقل 8 کاراکتر باشد و شامل حداقل یک عدد و یک حرف باشد.'
            })
    else:
        return Response({
            'status': False,
            'detail': 'شماره موبایل یا رمز عبرور وارد نشده است.'
        })


class LoginView(KnoxLoginView):  # login user.
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return super().post(request, format=None)


@ api_view(['POST'])
@permission_classes([AllowAny])
# send otp code to phonenumber for change password.(The user must be already registered.).
def phone_validation_passchange(request):
    phonenumber = request.POST.get('phonenumber')
    if phonenumber:
        user = User.objects.filter(phone__iexact=phonenumber)
        if user.exists():
            key = send_otp(phonenumber)
            if key:
                obj = OtpNumber.objects.filter(phonenumber=phonenumber)
                if obj:
                    objcount = obj.first()
                    if objcount.counts < 7:
                        # api = KavenegarAPI(
                        #     '7952507248584D75533155316553305A71497849324973554D465645566E474851547333537367396E56303D')
                        # params = {
                        #     'receptor': phonenumber,
                        #     'token': key,
                        #     'template': 'Verify',
                        #     'message': 'Kaveh specialized Web service '
                        # }
                        # response = api.verify_lookup(params)
                        objcount.counts += 1
                        objcount.otp_number = key
                        objcount.save()
                        return Response({
                            'status': True,
                            'detail':
                            'کد تایید ارسال شد.'
                        })
                    else:
                        return Response({
                            'status': False,
                            'detail':
                            'تعداد درخواست های تایید هویت پیامکی بیش از حد مجاز است لطفا با بخش پشتیبانی تماس فرمایید.'
                        })
                else:
                    # api = KavenegarAPI(
                    #     '7952507248584D75533155316553305A71497849324973554D465645566E474851547333537367396E56303D')
                    # params = {
                    #     'receptor': phonenumber,
                    #     'token': key,
                    #     'template': 'Verify',
                    #     'message': 'Kaveh specialized Web service '
                    # }
                    # response = api.verify_lookup(params)
                    OtpNumber.objects.create(
                        phonenumber=phonenumber, otp_number=key)
                    return Response({
                        'status': True,
                        'detail':
                            'کد تایید ارسال شد.'
                    })
            else:
                return Response({
                    'status': False,
                    'detail': 'send otp error.'
                })
        else:
            return Response({
                'status': False,
                'detail': 'كاربر ژيدا نشد.'
            })

    else:
        return Response({
            'status': False,
            'detail': 'شماره تلفن وارد نشده است.'
        })


@ api_view(['POST'])
@permission_classes([AllowAny])
def change_password(request):  # change password.
    phonenumber = request.POST.get('phonenumber')
    password = request.POST.get('password')
    if password and phonenumber:
        res = re.search(match_re, password)
        if res:
            myobj = OtpNumber.objects.filter(phonenumber=phonenumber)
            obj = myobj.first()
            if obj:
                if obj.validate:
                    user = User.objects.filter(phone__iexact=phonenumber)
                    if user.exists():
                        myuser = user.first()
                        myuser.set_password(password)
                        myuser.save()
                        obj.delete()
                        return Response({
                            'status': True,
                            'detail': 'پسورد با موفقيت تغيير كرد.'
                        })
                    else:
                        return Response({
                            'status': False,
                            'detail': 'كاربر پيدا نشد.'
                        })
                else:
                    return Response({
                        'status': False,
                        'detail': 'otp تایید نشده است.'
                    })
            else:
                return Response({
                    'status': False,
                    'detail': 'شماره موبایل پیدا نشد.'
                })
        else:
            return Response({
                'status': False,
                'detail': 'طول پسورد باید حداقل 8 کاراکتر باشد و شامل حداقل یک عدد و یک حرف باشد.'
            })
    else:
        return Response({
            'status': False,
            'detail': 'شماره موبایل یا رمز عبرور وارد نشده است.'
        })



@ api_view(['GET'])
@permission_classes([IsAuthenticated])
def userinfo(request):  # get user informations.
    user = User.objects.get(phone=request.user.get_username())
    if user:
        serializer = UserSerializer(user)
        return Response(serializer.data, status=status.HTTP_200_OK)
    else:
        return Response({
            'status': False,
            'detail': 'کاربر ثبت نام نکرده است.'
        })
        

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def edituserinfo(request): 
    key = request.POST.get('key')
    value = request.POST.get('value')
    user = User.objects.get(phone=request.user.get_username())
    if user and key and value:
        user.__dict__[key] = value
        user.save()
        return Response({
            'status': True,
            'detail': 'ویرایش با موفقیت انجام شد.',
            'value': value,
            'key': key
        })
    else:
        return Response({
            'status': False,
            'detail': 'کاربر پیدا نشد.'
        })
        
        
@api_view(['POST'])
@login_required
@permission_classes([IsAuthenticated])
def bookmark(request):
    if request.user.is_authenticated:
        obj_id = request.POST.get('id')
        if obj_id:
            if Bookmark.objects.filter(user=request.user, house_id=obj_id):
                Bookmark.objects.filter(user=request.user, house_id=obj_id).delete()
                return Response({
                    'status': True,
                    'detail': 'لایک برداشته شد.'
                })
            else:
                Bookmark.objects.create(user=request.user, house_id=obj_id)
                return Response({
                    'status': True,
                    'detail': 'لایک شد'
                })
        return Response({
            'status': False,
            'detail': 'یه مشکلی پیش اومده.'
        })
    else:
        return Response({
            'status': False,
            'detail': 'برای نشان کردن ابتدا باید در سایت ثبت نام کنید.'
        })
        
        
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def bookmarkdisplay(request):
    obj_id = request.POST.get('id')
    if obj_id and Bookmark.objects.filter(user=request.user, house_id=obj_id):
        return Response({'status': True, })
    else:
        return Response({'status': False, })



@api_view(['GET'])
@permission_classes([IsAuthenticated])
def getuserbookmark(request): 
    if request.user:
        obj = Bookmark.objects.filter(user=request.user)
        serializer = bookmarkserializer(obj, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    else:
        return Response({
            'status': False,
            'detail': 'کاربر پیدا نشد'
        })