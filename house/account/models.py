from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractUser
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _


class MyUserManager(BaseUserManager):
    def create_user(self, phone, password, **extra_fields):

        if not phone:
            raise ValueError('Users must have an phone number.')
        if not password:
            raise ValueError('Users must have an password.')

        user = self.model(phone=phone, **extra_fields)

        user.set_password(password)
        
        user.save()
        return user

    

    def create_superuser(self, phone, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(phone, password, **extra_fields)


class User(AbstractUser):
    username = None
    phone_regex = RegexValidator(
        regex=r'(\+98|0)?9\d{9}',
        message="phone number mustbe entered in the format:'+99999999'. up tho 14 digits allowed.")
    phone = models.CharField(validators=[phone_regex], max_length=15, unique=True)
    
    objects = MyUserManager()

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.phone

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    

class OtpNumber(models.Model):
    phonenumber = models.CharField(max_length=25, null=True)
    otp_number = models.CharField(max_length=25, null=True)
    counts = models.PositiveIntegerField(default=0, null=True, blank=True)
    validate = models.BooleanField(default=False)

    def __str__(self):
        return self.phonenumber



