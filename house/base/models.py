from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()


def image_path(instance, filename):
    return "%s/%s" % (instance.house.id, filename)



class House(models.Model):
    TYPE1 = (
        ('A', 'آپارتمان'),
        ('B', 'خانه و ویلا'),
        ('C', 'زمین و کلنگی'),
    )
    ROOMS = (
        ('0', '0'),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
    )
    
    FLOORS = (
        ('Z', 'زیر هم کف'),
        ('H', 'هم کف'),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
    )
    
    ADVERTISER = (
        ('A', 'املاک'),
        ('S', 'شخصی'),
    )
    
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=100, null=True)
    description = models.TextField(null=True)
    house_type = models.CharField(max_length=1, choices=TYPE1)  
    price = models.PositiveIntegerField(null=True)
    publish_date = models.DateTimeField(auto_now_add=True, null=True)
    size = models.PositiveSmallIntegerField(null=True)
    number_of_rooms = models.CharField(max_length=2, choices=ROOMS)
    floor = models.CharField(max_length=2, choices=FLOORS)
    building_age = models.PositiveSmallIntegerField(null=True)
    status = models.BooleanField(default=False)
    just_urgent = models.BooleanField(default=False)
    Advertiser = models.CharField(max_length=1, null=True, choices=ADVERTISER)
    
class HouseImage(models.Model):
    house = models.ForeignKey(House, related_name='items', on_delete=models.CASCADE)
    image = models.ImageField(upload_to=image_path, null=True)
    def __str__(self):
        return self.house.title
    
class Bookmark(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    house = models.ForeignKey(House, on_delete=models.CASCADE, null=True)
    
