from django.urls import path

from .views import homepage, Ads, detail, housedetail

urlpatterns = [
    path('', homepage, name='homepage'),
    path('getads/', Ads.as_view(), name='Ads'),
    path('housedetails/', housedetail, name='housedetail'),
    path('housedetail/<int:id>/', detail, name='detail'),
    
]
