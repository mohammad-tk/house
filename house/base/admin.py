from django.contrib import admin
from nested_inline.admin import NestedStackedInline, NestedModelAdmin
from .models import House, HouseImage, Bookmark

admin.site.register(HouseImage)


class HouseImageInline(NestedStackedInline):
    model = HouseImage
    extra = 1
    fk_name = 'house'

class HouseAdmin(NestedModelAdmin):
    model = House
    inlines = [HouseImageInline]
    list_display = (
        'user',
        'title',
        'Advertiser',
        'house_type',
        'price',
        'publish_date',
        'size',
        'number_of_rooms',
        'floor',
        'building_age',
        'status',
        'just_urgent',
    )
    search_fields = ('user','description',)
    list_filter = ('house_type', 'Advertiser', 'price','publish_date', 'size', 'number_of_rooms', 'floor', 'building_age','status', 'just_urgent')


admin.site.register(House, HouseAdmin)

