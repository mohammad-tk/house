from rest_framework import serializers
from .models import House, HouseImage
import locale
locale.setlocale(locale.LC_ALL, '')

class imageSerializer(serializers.ModelSerializer):
    class Meta:
        model = HouseImage
        exclude = ('house',)
        
class house_serializer(serializers.ModelSerializer):
    items = imageSerializer(many=True)
    price = serializers.SerializerMethodField()
    Advertiser = serializers.CharField(source='get_Advertiser_display')
    house_type = serializers.CharField(source='get_house_type_display')
    
    def get_price(self, obj):
        return f'{int(obj.price):n}'
    
    class Meta:
        model = House
        fields = '__all__'