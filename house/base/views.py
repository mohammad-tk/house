from django.shortcuts import render
from .serializers import house_serializer
from rest_framework.permissions import AllowAny
from .models import House
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.generics import ListAPIView
from django_filters.rest_framework import DjangoFilterBackend


def homepage(request):
    return render(request, 'home.html')

@permission_classes([AllowAny])
def detail(request, id):
    ctx = {}
    obj = House.objects.get(id=id)
    ctx['house_id'] = obj.id
    return render(request, 'detail.html', ctx)



class Ads(ListAPIView):
    serializer_class = house_serializer
    permission_classes = [AllowAny]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['size', 'price', 'building_age','number_of_rooms','Advertiser', 'floor']

    def get_queryset(self):
        size1 = self.request.GET.get('size1')
        size2 = self.request.GET.get('size2')
        price1 = self.request.GET.get('price1')
        price2 = self.request.GET.get('price2')
        building_age1 = self.request.GET.get('building_age1')
        building_age2 = self.request.GET.get('building_age2')
        number_of_rooms = self.request.GET.get('number_of_rooms')
        Advertiser = self.request.GET.get('Advertiser')
        floor = self.request.GET.get('floor')
        if price1 and price2:
            return House.objects.filter(price__gte=price1, price__lte=price2)
        elif size1 and size2:
            return House.objects.filter(size__gte=size1, size__lte=size2)
        elif building_age1 or building_age2:
            return House.objects.filter(building_age__gte=building_age1, building_age__lte=building_age2)
        elif number_of_rooms:
            return House.objects.filter(number_of_rooms=number_of_rooms)
        elif Advertiser:
            return House.objects.filter(Advertiser=Advertiser)
        elif floor:
            return House.objects.filter(floor=floor)        
        else:
            return House.objects.all()
        
        
        
        
        # def get_queryset(self):
        # order = self.request.GET.get('order')    
        # size1 = self.request.GET.get('size1')
        # size2 = self.request.GET.get('size2')
        # price1 = self.request.GET.get('price1')
        # price2 = self.request.GET.get('price2')
        # building_age1 = self.request.GET.get('building_age1')
        # building_age2 = self.request.GET.get('building_age2')
        # number_of_rooms = self.request.GET.get('number_of_rooms')
        # Advertiser = self.request.GET.get('Advertiser')
        # floor = self.request.GET.get('floor')
        # if price1 and price2:
        #     return House.objects.filter(price__gte=price1, price__lte=price2).order_by('order')
        # elif size1 and size2:
        #     return House.objects.filter(size__gte=size1, size__lte=size2).order_by('order')
        # elif building_age1 or building_age2:
        #     return House.objects.filter(building_age__gte=building_age1, building_age__lte=building_age2).order_by('order')
        # elif number_of_rooms:
        #     return House.objects.filter(number_of_rooms=number_of_rooms).order_by('order')
        # elif Advertiser:
        #     return House.objects.filter(Advertiser=Advertiser).order_by('order')
        # elif floor:
        #     return House.objects.filter(floor=floor).order_by('order')        
        # else:
        #     return House.objects.all().order_by('order')



@ api_view(['GET'])
@permission_classes([AllowAny])
def housedetail(request):
    obj_id = request.GET.get('id')
    if obj_id and obj_id.isdigit():
        obj = House.objects.get(id=obj_id)
        serializer = house_serializer(obj)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)



